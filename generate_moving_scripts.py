#!/usr/bin/env python3

import csv
import os


ORPHANS_MOVE_LINE = 'move {0} {1}\n'
SVN_MOVE_LINE = ('if [ -e {0}t ]; then [ ! -d {1} ] && '
                 'svn mkdir --parents {1}; '
                 'svn mv {0}t {2}t; fi\n')


def write_script(msg_type, msg_file):
    """Write the template mover script and the process_orphans
    input file."""
    move_template_script = 'move_templates_{0}.sh'.format(msg_type)
    move_orphans_file = 'move_orphans_{0}.txt'.format(msg_type)
    with open(msg_file, newline='') as csvfile, \
            open(move_template_script, 'w') as move_templates_script, \
            open(move_orphans_file, 'w') as move_orphans:
        msg_reader = csv.DictReader(csvfile, delimiter=",", quotechar='"')
        for msg_row in msg_reader:
            # messages/kdemultimedia/juk.pot -> messages/juk/juk.pot
            source_file = os.path.join(msg_type, msg_row['module'],
                                       msg_row['pot']+'.po')
            destination_dir = os.path.join(msg_type, msg_row['repository'])
            destination_file = os.path.join(destination_dir,
                                            msg_row['pot']+'.po')

            if source_file == destination_file:
                print('Skipping {}, no move needed'.format(source_file))
                continue

            move_templates_script.write(
                SVN_MOVE_LINE.format(source_file, destination_dir, destination_file)
            )
            move_orphans.write(ORPHANS_MOVE_LINE.format(source_file,
                                                        destination_file))


def main():
    write_script('messages', 'kde_pots_location_msg.csv')
    write_script('docmessages', 'kde_pots_location_doc.csv')


if __name__ == '__main__':
    main()
