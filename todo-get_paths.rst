===============================
Analysis of get_paths functions
===============================

Terminology
-----------

`<reponame>`
  the repository name before the transition,
  which used to be unique but it may not be anymore

`<module>`
  the old, deprecated module name
  (i.e. extragear-network, kde-workspace, playground-graphics)

`<repoid>`
  the new repository ID which is meant to be unique
  (at the conversion time, the value of `<reponame>`)

`<real_repo_path>`
  the new namespace for the repository
  (i.e. graphics, websites, wikitolearn, frameworks, etc)

Module list
-----------

The `list_modules` currently parses kde_projects.xml
and returns a list of <module>_<reponame>.

It will return a list of <repoid>.

Other functions
---------------

Input for all of the functions: <module>_<reponame>,
which are the items returned by `list_modules`.

get_path
  current: git-(un)stable(-kf5)/<module>_<reponame> (or svn repo)
  new: git-(un)stable(-kf5)/<repoid> (or svn repo)
 
get_po_path
  - current: <module> 
    (with some special rules to account for workspace -> kde-workspace,
    www-*|websites_* -> www, kdesupport-* -> kdesupport)
  - new: <repoid>

get_vcs
  - current: git or svn
  - new: git by default, svn for www-www (special exception when www-www
    is specified)
 
get_branch
  - current: <branch>
  - new: <branch> (no changes; in the future, get the branch from
    repo-metadata)
  
get_repo_name
  - current: <reponame>
  - new: <repoid>
  
get_url:
  - current: kde:<reponame>.git/kdei:<reponame>.git/kdep:websites/<reponame>.git
  - new: kdei:<real_repo_path>.git

Summary
-------

- `get_repo_name` and `get_po_path` are going to be the same
  (most likely `get_repo_name`, which is only used inside `get_path`,
   be replaced/become an alias for the other);
- `get_path` will use get_repo_name to build the checkout path
