#!/bin/bash

# Copy the data to the right place
# Run this script from the root of each l10n branch

OUTFILE="move_data.sh"
:>${OUTFILE}

T=data

for L in $(cat subdirs); do
    # this breaks for filename with newlines, but we don't have any
    if [ ! -d ${L}/${T} ]; then
        echo "${L}: ${T} not found, skipping"
        continue
    fi
    echo "${L}: moving ${T}"
    find ${L}/${T}/ -mindepth 2 -maxdepth 2 | while read F; do
        # Special handling: exclude special items from the move,
        # and either keep them in place (it is not clear now
        # where they should land).
        if [[ ${F} =~ .*global/autocorrect$ ]] ||
            [[ ${F} =~ .*kdelibs/autocorrect$ ]]; then
            printf "\tskipping ${F}\n"
            continue
        fi
        echo "svn mv ${F} ${L}/${T}" >>${OUTFILE}
    done
done

