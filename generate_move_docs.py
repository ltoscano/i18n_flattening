#!/usr/bin/env python3

"""
Read scripts/documentation_paths and create moving rules
for the documentation.

This script must be executed from the root of each i18n
branch.
"""

from collections import OrderedDict
import re
import os


RULE_SHELL_ITEM = """
if [ -d {source_dir} ]; then
    if [ ! -d "{dest_dir}" ]; then
       svn mkdir --parents {dest_dir}
    fi
    svn mv {source_dir} {dest_dir}
fi"""


def read_move_doc_rules():
    moving_rules = OrderedDict()
    current_module = ''

    with open('scripts/documentation_paths', 'r') as docpaths:

        for rule_line in docpaths:
            rule_elements = rule_line.split()
            if len(rule_elements) < 2:
                continue

            if rule_elements[0] == 'module':
                current_module = rule_elements[1]
                continue

            if rule_elements[0] == 'entry':
                if len(rule_elements) < 3 or len(rule_elements) > 4:
                    continue

                doc_dir = rule_elements[1]
                if doc_dir in ['kioslave', 'kioslave5', 'kcontrol',
                               'kcontrol5']:
                    print('Skipping {}/{}, too generic; please handle it '
                          'manually'.format(current_module, doc_dir))
                    continue

                repository_name = rule_elements[2]
                if current_module == repository_name:
                    print('Skipping {}/{}, same source and destination ({})'
                          .format(current_module, doc_dir, repository_name))
                moving_rules[(current_module, doc_dir)] = repository_name

    # a bit of hardcoded rules
    moving_rules[('kde-workspace', 'kcontrol5/filetypes')] = 'kde-cli-tools'
    moving_rules[('kde-workspace', 'kcontrol/autostart')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/baloo')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/clock')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/colors')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/componentchooser')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/cursortheme')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/desktopthemedetails')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/emoticons')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/fontinst')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/fonts')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/formats')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/icons')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/joystick')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/kcmaccess')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/kcmlaunchfeedback')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/kcmsmserver')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/kcmstyle')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/kded')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/keyboard')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/keys')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/mouse')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/notifications')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/paths')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/solid-actions')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/solid-device-automounter')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/spellchecking')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/splashscreen')] = 'plasma-desktop'
    moving_rules[('kde-workspace', 'kcontrol/translations')] = 'plasma-workspace'
    moving_rules[('kde-workspace', 'kcontrol/workspaceoptions')] = 'plasma-desktop'
    moving_rules[('applications', 'kcontrol5/bookmarks')] = 'konqueror'
    moving_rules[('applications', 'kcontrol5/filemanager')] = 'konqueror'
    moving_rules[('applications', 'kcontrol5/history')] = 'konqueror'
    moving_rules[('applications', 'kcontrol5/kcmcss')] = 'konqueror'
    moving_rules[('applications', 'kcontrol5/khtml-adblock')] = 'konqueror'
    moving_rules[('applications', 'kcontrol5/khtml-behavior')] = 'konqueror'
    moving_rules[('applications', 'kcontrol5/khtml-general')] = 'konqueror'
    moving_rules[('applications', 'kcontrol5/khtml-java-js')] = 'konqueror'
    moving_rules[('applications', 'kcontrol5/performance')] = 'konqueror'
    moving_rules[('kdenetwork', 'kioslave5/bookmarks')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/bzip2')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/fish')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/gzip')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/info')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/man')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/network')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/nfs')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/recentdocuments')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/sftp')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/smb')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/tar')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/thumbnail')] = 'kio-extras'
    moving_rules[('kdenetwork', 'kioslave5/xz')] = 'kio-extras'
    moving_rules[('frameworks', 'kcontrol5/cache')] = 'kio'
    moving_rules[('frameworks', 'kcontrol5/cookies')] = 'kio'
    moving_rules[('frameworks', 'kcontrol5/kcm_ssl')] = 'kdelibs4support'
    moving_rules[('frameworks', 'kcontrol5/netpref')] = 'kio'
    moving_rules[('frameworks', 'kcontrol5/proxy')] = 'kio'
    moving_rules[('frameworks', 'kcontrol5/smb')] = 'kio'
    moving_rules[('frameworks', 'kcontrol5/trash')] = 'kio'
    moving_rules[('frameworks', 'kcontrol5/useragent')] = 'kio'
    moving_rules[('frameworks', 'kcontrol5/webshortcuts')] = 'kio'
    moving_rules[('frameworks', 'kioslave5/data')] = 'kio'
    moving_rules[('frameworks', 'kioslave5/file')] = 'kio'
    moving_rules[('frameworks', 'kioslave5/ftp')] = 'kio'
    moving_rules[('frameworks', 'kioslave5/help')] = 'kio'
    moving_rules[('frameworks', 'kioslave5/http')] = 'kio'
    moving_rules[('frameworks', 'kioslave5/mailto')] = 'kio'
    moving_rules[('frameworks', 'kioslave5/telnet')] = 'kio'
    moving_rules[('frameworks', 'kioslave5/webdav')] = 'kio'

    return moving_rules


def generate_move_doc_rule(moving_rules, rule_file_name):
    # languages_file_name = os.path.join(os.getcwd(), 'subdirs')

    with open(rule_file_name, 'w') as rulefile, \
            open('subdirs', 'r') as languages_file:
        for lang in languages_file:
            lang = lang.strip()
            doc_basedir = os.path.join(lang, 'docs')
            for source, dest in moving_rules.items():
                source_dir = os.path.join(doc_basedir, source[0], source[1])

                # if source[1] (the original repository directory) only
                # contains a simple directory, the last item added
                # is empty, and the rule works in any case.
                dest_dir = os.path.join(
                    doc_basedir, dest, os.path.dirname(source[1])
                )

                rulefile.write(RULE_SHELL_ITEM.format(
                    source_dir=source_dir,
                    dest_dir=dest_dir)
                )


def main():
    moving_rules = read_move_doc_rules()
    outfile_name = 'move_docs_rules.sh'
    generate_move_doc_rule(moving_rules, outfile_name)


if __name__ == '__main__':
    main()
